//Import dependencies
const express = require('express')
const fs = require('fs');
const http = require('http')
const https = require('https');
const bodyParser = require('body-parser')
const shell = require('shelljs')
const cors = require('cors')
const { spawn } = require('child_process')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const jwt2 = require('express-jwt');
const hcx = require('./hcx/index.js')
const scheduler = require('node-schedule');

// Authen
var auth = jwt2({
      secret: 'MY_SECRET',
      userProperty: 'payload'
    });


//Init express
const app = express()
/*
const app = require('fastify')({
      https: credentials
}
);*/
const port = process.env.PORT || 443;
app.set('port', port)

//Enable bodyParser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:false}))
 
//Enable CORS
app.use(cors())

//Enable Vcn-App Content
app.use('/', express.static('/var/www/html/vcn-app/dist/vcn-app'));

// MongoDB
const db_host = process.env.DB || 'localhost';
const urlmongo = `mongodb://${db_host}:27017/vcndb`; 
mongoose.connect(urlmongo);
var db = mongoose.connection; 
db.on('error', console.error.bind(console, 'Erreur lors de la connexion'))
db.once('open', function (){
    console.log("Connexion à la base OK")
})

const UserSchema = new mongoose.Schema({
      username: {
            type: String,
            required: true
      },
      password: {
            type: String,
            required: true
      }})

const LogSchema = new mongoose.Schema({
      user: {
            type: String,
            required: false
      },
      date: {
            type: Date,
            required: true
      },
      task: {
            type: String,
            required: true
      },
      cloud: {
            type: String,
            required: false
      }})

const ScheduleSchema = new mongoose.Schema({
      id: {
            type: String,
            required: false
      },
      action: {
            type: String,
            required: false
      },
      completed: {
            type: Boolean,
            required: false
      },
      date: {
            type: Date,
            required: true
      },
      config: {
            type: Map,
            required: true
      }})

UserSchema.methods.generateJwt = function() {
      var expiry = new Date();
      expiry.setDate(expiry.getDate() + 7);
    
      return jwt.sign({
        _id: this._id,
        username: this.username,
        password: this.password,
        exp: parseInt(expiry.getTime() / 1000),
      }, "MY_SECRET"); // DO NOT KEEP YOUR SECRET IN THE CODE!
    };

UserSchema.methods.validPassword = function(password) {
      return this.password === password;
    };

var User = mongoose.model('User', UserSchema)
var Log = mongoose.model('Log', LogSchema)
var Schedule = mongoose.model('Schedule', ScheduleSchema)

// Passport
passport.use(new LocalStrategy({
      usernameField: 'username'
    },
    function(username, password, done) {
      User.findOne({ username: username }, function (err, user) {
        if (err) { return done(err); }
        // Return if user not found in database
        if (!user) {
          return done(null, false, {
            message: 'User not found'
          });
        }
        // Return if password is wrong
        if (!user.validPassword(password)) {
          return done(null, false, {
            message: 'Password is wrong'
          });
        }
        // If credentials are correct, return the user object
        return done(null, user);
      });
    }
  ));

  profileRead = function(req, res) {

      // If no user ID exists in the JWT return a 401
      if (!req.payload._id) {
        res.status(401).json({
          "message" : "UnauthorizedError: private profile"
        });
      } else {
        // Otherwise continue
        User
          .findById(req.payload._id)
          .exec(function(err, user) {
            res.status(200).json(user);
          });
      }
    
    };



// Router
var router = express.Router()
app.use(passport.initialize());
app.use(router)

// Get Progress
var logs = [""]
var completed = false
var changes = 0
var lastline = ""



function DbLog(task, cloud, user) {
      var dblog = new Log();
      dblog.task = task
      dblog.cloud = cloud
      dblog.user = user
      dblog.date = new Date()
      dblog.save(function(err){
            console.log(dblog)
            if(err){
              console.log(err);
            }
          })
}

router.post('/api/terraform/plan', auth);
router.route('/api/terraform/plan').post(async function(req,res){

      completed = false
      let optString = ''
      console.log(req.body)

      Object.keys(req.body).forEach((option) => {
            console.log(req.body[option])
            optString += ` -var '${option}=${req.body[option]}'`
      });
            
      let command = `terraform plan -lock=false -no-color${optString}`
      console.log(command)

      shell.cd(`/root/${req.body.id}`)
      DbLog("plan", `${req.body.id}`, `${req.header('User')}` )
      let child = shell.exec(command)

      console.log(child.stderr)

      if (child.stderr != "")
      {
            completed = true
            res.sendStatus(500)
      }
            
      else {
            let output = child.stdout.match(/Plan: [0-9]* to add, [0-9]* to change, [0-9]* to destroy/g)
            let index = child.stdout.indexOf("Terraform will perform the following actions:") + 47
            let index2 = child.stdout.indexOf("Note:") - 76
            if (output) {
                  let tocreate = Number(output[0].split(" ")[2 - 1])
                  let toupdate = Number(output[0].split(" ")[5 - 1])
                  let todelete = Number(output[0].split(" ")[8 - 1])
                  let tochange = tocreate + toupdate + todelete

                  console.log(tochange)
                  console.log(tocreate)
                  console.log(toupdate)
                  console.log(todelete)

                  res.json({changes: tochange, tocreate: tocreate, toupdate: toupdate, todelete: todelete, output: child.stdout.substring(index, index2)})
            }
            else {
                  res.json({changes: 0, tocreate: 0, toupdate: 0, todelete: 0, output: "" })  
            }        
      }     
})

router.post('/api/terraform/plan-destroy', auth);
router.route('/api/terraform/plan-destroy').post(async function(req,res){

      let optString = ''
      console.log(req.body)

      Object.keys(req.body).forEach((option) => {
            console.log(req.body[option])
            optString += ` -var '${option}=${req.body[option]}'`
      });

      let command = `terraform plan -destroy -no-color${optString}`
      console.log(command)

      shell.cd(`/root/${req.body.id}`)
      DbLog("plan", `${req.body.id}`, `${req.header('User')}` )
      let child = shell.exec(command);

      console.log(child.stderr)

      if (child.stderr != "")
            res.sendStatus(500)
      else {
            let output = child.stdout.match(/Plan: [0-9]* to add, [0-9]* to change, [0-9]* to destroy/g)
            if (output) {
                  let toadd = output[0].split(" ")[2 - 1]
                  let tochange = output[0].split(" ")[5 - 1]
                  let todelete = output[0].split(" ")[8 - 1]

                  console.log(toadd)
                  console.log(tochange)
                  console.log(todelete)

                  res.json({toadd: toadd, tochange: tochange, todelete: todelete});
            }
            else {
                  res.json({toadd: 0, tochange: 0, todelete: 0})  
            } 
      }     
})

router.post('/api/terraform/apply', auth);
router.route('/api/terraform/apply').post(async function(req,res){

      // Reset counters
      logs = [""]
      lastline = ""
      completed = false
      changes = 0

      var optString = ['apply', '-auto-approve', '-no-color']
      console.log(req.body)

      Object.keys(req.body).forEach((option) => {
            console.log(req.body[option])
            optString.push('-var')
            optString.push(`${option}=${req.body[option]}`)
      })

      let command = `terraform`
      console.log(command)
      console.log(optString)

      shell.cd(`/root/${req.body.id}`)
      DbLog("apply", `${req.body.id}`, `${req.header('User')}` )
      let child = spawn(command, optString)
      
      logs = [command + " " + optString.join(" ")]

      child.stdout.on('data', function(data) {
            let s = data.toString()
            if (s.search('Creation complete') != -1) {changes += 1}
            if (s.search('Modifications complete') != -1) {changes += 1}
            if (s.search('Destruction complete') != -1) {changes += 1}
            if (s.search('Apply complete!') != -1) {completed = true}

            console.log(s)
            lastline = s.replace(/[^\w ]/, '')
            logs = logs.concat(lastline.split('\n')).filter(line => line != "")
            //logs += lastline
      } )

      

      child.stderr.on('data', (data) => {
            let s = data.toString()
            lastline = s.replace(/[^\w ]/, '')
            logs = logs.concat(lastline.split('\n')).filter(line => line != "")
            completed = true
          });
     
      res.json({status: 'ok'})
})

router.post('/api/schedule/apply', auth);
router.route('/api/schedule/apply').post(async function(req,res){

      console.log('/api/schedule/apply');
      var schedule = new Schedule();
      schedule.id = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
      schedule.action = req.body['action']
      schedule.date = req.body['date']
      schedule.config = req.body['config']
      schedule.completed = false
      
      // Record the task to schedule
      schedule.save(function(err){
            console.log(schedule)
            if(err){
              console.log(err);
            }
          })
      
      // Schedule the task in node js
      var j = scheduler.scheduleJob(schedule.date, function(y){
            console.log(schedule);

            var optString = [`${schedule.action}`, '-auto-approve', '-no-color']
            console.log(schedule.config)

            for (var entry of schedule.config.entries()) {
                  var key = entry[0],
                      value = entry[1];
                  console.log(key + " = " + value);
                  optString.push('-var')
                  optString.push(`${key}=${value}`)
              }

            let command = `terraform`
            console.log(command)
            console.log(optString)

            shell.cd(`/root/${schedule.config.get('id')}`)
            DbLog(`${schedule.action}`, `${schedule.config.get('id')}`, `scheduled task` )
            let child = spawn(command, optString)

            // Update the list
            Schedule.updateOne({ id: schedule.id }, { completed: true }, function(err, res) {
                  console.log(res)
                });
          }.bind(null,schedule));
      res.json({status: 'ok'})
})


router.post('/api/schedule/list', auth);
router.route('/api/schedule/list').post(async function(req,res){
      const schedules = await Schedule.find().sort('-date').limit(1000);
      res.json(schedules);  
})


router.post('/api/schedule/delete/:schedule_id', auth);
router.route('/api/schedule/delete/:schedule_id').post(async function(req,res){
      const schedules = await Schedule.deleteOne({ id: req.params.schedule_id });
      res.json(schedules);  
})

router.post('/api/terraform/output', auth);
router.route('/api/terraform/output').post(async function(req,res){ 
      res.send(logs)
})

router.post('/api/terraform/progress', auth);
router.route('/api/terraform/progress').post(async function(req,res){
      res.json({changes: changes, completed: completed, lastline: lastline, fullline: logs /*.slice().reverse()*/})
})

router.post('/api/terraform/destroy', auth);
router.route('/api/terraform/destroy').post(async function(req,res){

      // Reset counters
      logs = [""]
      lastline = ""
      completed = false
      changes = 0

      console.log(req.body)

      let command = `terraform`
      shell.cd(`/root/${req.body.id}`)
      DbLog("destroy", `${req.body.id}`, `${req.header('User')}` )
      let child = spawn(command, ['destroy', '-auto-approve', '-no-color'])

      child.stdout.on('data', function(data) {
            let s = data.toString()
            if (s.search('Destruction complete') != -1) {console.log('Found complete'); changes += 1}
            if (s.search('Destroy complete!') != -1) {completed = true}

            console.log(s)
            lastline = s.replace(/[^\w ]/, '')
            logs = logs.concat(lastline.split('\n')).filter(line => line != "")
      } )

      child.stderr.on('data', (data) => {
            let s = data.toString()
            lastline = s.replace(/[^\w ]/, '')
            logs = logs.concat(lastline.split('\n')).filter(line => line != "")
            completed = true
          });
     
      res.json({status: 'ok'})
})

router.post('/api/sdwan/list', auth);
router.route('/api/sdwan/list').post(async function(req,res){

      let command = `/root/sdwan/get-edges.sh`
      let child = shell.exec(command)

      if (child.stderr != "")
      {
            console.log(child.stderr);
            res.sendStatus(500);
      }
            
      else {
            res.send(child.stdout);           
      }     
})

router.post('/api/appdefense/list', auth);
router.route('/api/appdefense/list').post(async function(req,res){
    
      let command = `/root/ios/getappd.sh`
      let child = shell.exec(command)
      
      if (child.stderr != "")
      {
            console.log(child.stderr);
            res.sendStatus(500);
      }           
      else {
            res.send(child.stdout);          
      }  
        
})

router.route('/api/login').post(async function(req,res){ 
      passport.authenticate('local', function(err, user, info){
            var token;
        
            // If Passport throws/catches an error
            if (err) {
              res.status(404).json(err);
              return;
            }
        
            // If a user is found
            if(user){
              token = user.generateJwt();
              res.status(200);
              DbLog("login", "", user.username)
              res.json({
                "token" : token
              });
            } else {
              // If user is not found
              res.status(401).json(info);
            }
          })(req, res);
})

router.route('/api/register').post(async function(req,res){ 
      var user = new User()
      user.username = "admin"
      user.password = "VMware1!"
      user.save(function(err){
            var token;
            token = user.generateJwt();
            res.status(200);
            res.json({
                  "token" : token
            });
            if(err){
              res.send(err);
            }
          })
})

router.post('/api/hcx', auth);
router.route('/api/hcx').post(function(req,res){ 
  hcx.migrate();
  res.json({status: 'ok'})   
})


router.post('/api/dashboard', auth);
router.route('/api/dashboard').post(async function(req,res){
      const countFactory = await Log.where({'cloud':'factory'}).countDocuments();
      const countPks = await Log.where({'cloud':'pks'}).countDocuments();
      const countCloud = await Log.where({'cloud':'aws'}).countDocuments();
      const countVMC = await Log.where({'cloud':'vmc'}).countDocuments();
      const countPlan = await Log.where({'task':'plan'}).countDocuments();
      const countApply = await Log.where({'task':'apply'}).countDocuments();
      const countDestroy = await Log.where({'task':'destroy'}).countDocuments();
      console.log(countFactory)
      res.json({countFactory: countFactory, countPks: countPks, countCloud: countCloud, countVMC: countVMC, countPlan: countPlan, countApply: countApply, countDestroy: countDestroy});   
})

router.post('/api/logs', auth);
router.route('/api/logs').post(async function(req,res){
      const logs = await Log.find().sort('-date').limit(1000);
      res.json(logs);   
})

app.use(function(req, res, next) {
      res.status(404).redirect('/');
    });


if (port == 443) {
      // Certificate
      const privateKey = fs.readFileSync('/root/server/certs/private.key', 'utf8');
      const certificate = fs.readFileSync('/root/server/certs/vcn.cloud_ssl_certificate.cer', 'utf8');

      const credentials = {
            key: privateKey,
            cert: certificate,
      };

      //Create the API server.
      const server = https.createServer(credentials, app);
      DbLog("https server started");
      server.listen(port, () => console.log(`VCN API running on localhost:${port}`));
} else {
      const server = http.createServer(app);
      DbLog("http server started");
      server.listen(port, () => console.log(`VCN API running on localhost:${port}`));
}


