
var parseHttpHeader = require('parse-http-header');
const request = require('request');

let server = 'hcx-mgr.branch.vcn.lab';
var headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
  'x-hm-authorization': ''
};

var source = {
  'endpointId': '',
  'endPointName': '',
  'endpointType': '',
  'resourceId': '',
  'resourceName': '',
  'resourceType': ''  
}

var destination = {
  'endpointId': '',
  'endPointName': '',
  'endpointType': '',
  'resourceId': '',
  'resourceName': '',
  'resourceType': ''  
}

var entityDetails = {
  'entityId': '',
  'entityName': ''
}

var placement = [{
  'containerId': "resgroup-44",
  'containerName': "Compute-ResourcePool",
  'containerType': "resourcePool"
},
{
  'containerId': "datacenter-3",
  'containerName': "SDDC-Datacenter",
  'containerType': "dataCenter"
},
{
  'containerId': "group-v97",
  'containerName': "Migrated VMs",
  'containerType': "folder"
}]

var targetNetworks = [{
  'destNetworkDisplayName': "L2E_hcx-test-71699-1700e87b",
  'destNetworkHref': "/infra/tier-1s/cgw/segments/L2E_hcx-test-71699-1700e87b",
  'destNetworkName': "L2E_hcx-test-71699-1700e87b",
  'destNetworkType': "VirtualNetwork",
  'destNetworkValue': "/infra/tier-1s/cgw/segments/L2E_hcx-test-71699-1700e87b",
  'srcNetworkDisplayName': "hcx-ls",
  'srcNetworkHref': "network-o252",
  'srcNetworkName': "hcx-ls",
  'srcNetworkType': "OpaqueNetwork",
  'srcNetworkValue': "network-o252"
}]

var input = {
  'decisionRules': {
    'forcePowerOffVm': false,
    'removeISOs': true,
    'removeSnapshots': true,
    'upgradeHardware': false,
    'upgradeVMTools': false
  },
  'destination': destination,
  'entityDetails': entityDetails,
  'migrationType': 'vMotion',
  //'migrationType': 'VR',
  'networks': {
    'retainMac': false,
    'targetNetworks': targetNetworks
  },
  'placement': placement,
  'source': source,
  'schedule': {},
  'storage': {
    'datastoreId': "datastore-45",
    'datastoreName': "WorkloadDatastore",
    'diskProvisionType': "sameAsSource",
    'storageProfile': {
      'type': "default"
    }
  }
}

var migration = {
  'migrations': [{'input': input}]
}



function ConnectHcx() {
  return new Promise((resolve, reject) => {
    let options = {
      uri: 'https://hcx-mgr.branch.vcn.lab/hybridity/api/sessions',
      port: 443,
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
      },
      rejectUnauthorized: false,
      body: '{"username": "administrator@vsphere.local", "password": "VMware1!"}'
    };
  
    request(options, (error, response, body) => {
      if (error) {
        reject(error);
        return
      };
      if (response.statusCode != 200) {
          reject('Invalid status code <' + response.statusCode + '>');
      }
      resolve(parseHttpHeader(response.headers['x-hm-authorization'])[0]);
  });
  });
}

function GetCloudConfig() {
  return new Promise((resolve, reject) => {
    let options = {
      uri: 'https://hcx-mgr.branch.vcn.lab/hybridity/api/cloudConfigs',
      port: 443,
      method: 'GET',
      headers: headers,
      rejectUnauthorized: false,
    };
  
    request(options, (error, response, body) => {
      if (error) reject(error);
      if (response.statusCode != 200) {
          reject('Invalid status code <' + response.statusCode + '>');
      }
      resolve(body);  
  });
  });
}

function GetHcxEndpoint() {
  return new Promise((resolve, reject) => {
    let options = {
      uri: 'https://hcx-mgr.branch.vcn.lab/hybridity/api/service/inventory/resourcecontainer/list',
      port: 443,
      method: 'POST',
      headers: headers,
      rejectUnauthorized: false,
      body: '{"cloud": {"local": true, "remote": true}}'
    };
  
    request(options, (error, response, body) => {
      if (error) reject(error);
      if (response.statusCode != 200) {
          reject('Invalid status code <' + response.statusCode + '>');
      }
      resolve(body);  
  });
  });
}

function GetVirtualMachines() {
  return new Promise((resolve, reject) => {
    let options = {
      uri: 'https://hcx-mgr.branch.vcn.lab/hybridity/api/service/inventory/virtualmachines',
      port: 443,
      method: 'POST',
      headers: headers,
      rejectUnauthorized: false,
      body: '{"filter": {"container": []}}'
    };
  
    request(options, (error, response, body) => {
      if (error) reject(error);
      if (response.statusCode != 200) {
          reject('Invalid status code <' + response.statusCode + '>');
      }
      resolve(body);  
  });
  });
}

function StartMigration() {
  console.log('MIGRATION STARTED');
  return new Promise((resolve, reject) => {
    let options = {
      uri: 'https://hcx-mgr.branch.vcn.lab/hybridity/api/migrations?action=start',
      port: 443,
      method: 'POST',
      headers: headers,
      rejectUnauthorized: false,
      body: JSON.stringify(migration)
    };
  
    request(options, (error, response, body) => {
      if (error) reject(error);
      if (response.statusCode != 200) {
        console.log(response.body)
          reject('Invalid status code <' + response.statusCode + '>');
      }
      resolve(body);  
  });
  });
}

async function Hcx() {
  try {

      // Authentication
      const token = await ConnectHcx();
      headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'x-hm-authorization': token
      };
      console.log('AUTHENTICATION DONE');
      console.log(headers);

      // Cloud Config
      const cloudconfig = await GetCloudConfig();
      console.log('CLOUD CONFIG RETRIEVED');
      console.log(cloudconfig);
      var jsonObject = JSON.parse(cloudconfig);
      destination.endpointId = jsonObject.data.items[0].endpointId;
      destination.endPointName = jsonObject.data.items[0].name;
      destination.endpointType = "VC";
      destination.resourceId = "56d58607-fd5c-4206-84cb-acb4ed88276f";
      destination.resourceType = "VC";
      destination.resourceName = "vcenter.sddc-18-195-38-9.vmwarevmc.com";
      
      // EndPoint
      const endpoint = await GetHcxEndpoint();
      console.log('ENDPOINT RETRIEVED');
      console.log(endpoint)
      jsonObject = JSON.parse(endpoint);
      source.resourceId = jsonObject.data.items[0].resourceId;
      source.resourceType = "VC";
      source.resourceName = jsonObject.data.items[0].resourceName;
      source.endpointId = jsonObject.data.items[0].endpoint.endpointId;
      source.endPointName = jsonObject.data.items[0].endpoint.name;
      source.endpointType = "VC";

      // Get VM
      const vm = await GetVirtualMachines();
      console.log('VM CONFIG RETRIEVED');
      jsonObject = JSON.parse(vm);
      var items = jsonObject.data.items;
      var output = items.filter(function(value){ return value.name=="Tiny Linux VM";});
      entityDetails.entityId = output[0].id;
      entityDetails.entityName = output[0].name;
      console.log(output);

      console.log(input);
      console.log(JSON.stringify(migration));

      // Migration
      const start = await StartMigration();
      console.log(start)

  } catch (error) {
      console.error('ERROR:');
      console.error(error);
  }
}

//Hcx();
exports.migrate = function () {
  return Hcx();
}


